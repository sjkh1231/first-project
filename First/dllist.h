class DLLElement
{

 public: 
    DLLElement(int node_Key ); 
    DLLElement *next; 
    DLLElement *prev; 
    int key;        

};

class DLList 
{
 
public: 
    DLList(); 
    ~DLList(); 
    void Prepend(DLLElement *item); 
    void Append(DLLElement *item); 
    DLLElement  *Remove(); 
    void showlist();
    bool IsEmpty(); 
    void SortedInsert(DLLElement *item, int sortkey);
    DLLElement *SortedRemove(int sortKey); 

    private: 
    DLLElement *first;
    DLLElement *last;

};
void Insertlist(int n,DLList *list);
void Removelist(int n,DLList *list);
